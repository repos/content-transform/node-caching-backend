# Nodejs caching backend

> Project status: Prototype

A lightweight abstraction layer for adding cache to nodejs based web services.

## Engines

Currently supported engines

- Memory based (dev only)
- Cassandra
- Memcache

## Local setup

Requirements

- docker
- docker-compose
- node

To run the tests:

```
docker compose up tests
```
