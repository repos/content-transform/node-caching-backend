"use strict";
const { describe, beforeEach, afterEach, it } = require("mocha");
const axios = require("axios");
const assert = require("assert");
const express = require("express");
const cachedFactory = require("../lib/express_middleware");
const CacheBackend = require("../lib/backend");

let app;
let server;

describe("Express middleware tests", function () {
  this.timeout(10000);
  beforeEach((done) => {
    app = express();
    const port = 3000;
    const host = "127.0.0.1";
    const cacheTTL = 10;

    app.cache = {
      backend: new CacheBackend({ engine: "dummy" }),
    };

    app.get("/example", cachedFactory(cacheTTL), (req, res) => {
      res.send("Hello World!");
    });
    server = app.listen(port, host, () => {
      console.log(`Example app listening on port ${port}`);
      done();
    });
  });

  afterEach((done) => {
    server.close(function () {
      console.log("Shutting down test server");
      done();
    });
  });

  it("Should cache new response", async function () {
    const res = await axios.get("http://127.0.0.1:3000/example");
    const testCache = app.cache.backend.engine.cache;
    const testCacheObj = JSON.parse(testCache.data["/example"].v);
    assert(res.status === 200);
    assert(testCacheObj.body === "Hello World!");
  });

  it("Should return cached response", async function () {
    const firstRes = await axios.get("http://127.0.0.1:3000/example");
    const secondRes = await axios.get("http://127.0.0.1:3000/example");
    assert(firstRes.status === 200);
    assert(secondRes.status === 200);
    const testCache = app.cache.backend.engine.cache;
    const testCacheObj = JSON.parse(testCache.data["/example"].v);
    assert(testCacheObj.body === "Hello World!");
    assert(testCache.stats.hits === 1);
    assert(testCache.stats.misses === 1);
  });

  it("Should return cached headers", async function () {
    app.get(
      "/custom-headers",
      cachedFactory(10, (req) => `${req.originalUrl}-${req.method}`),
      (req, res) => {
        res.set("x-custom-header-1", "custom-value-1");
        res.set("x-custom-header-2", "custom-value-2");
        res.send("Custom headers");
      }
    );
    const firstRes = await axios.get("http://127.0.0.1:3000/custom-headers");
    const secondRes = await axios.get("http://127.0.0.1:3000/custom-headers");
    assert(firstRes.status === 200);
    assert(secondRes.status === 200);
    const testCache = app.cache.backend.engine.cache;
    const testCacheObj = JSON.parse(testCache.data["/custom-headers-GET"].v);
    assert(testCacheObj.body === "Custom headers");
    assert("x-custom-header-1" in testCacheObj.headers);
    assert("x-custom-header-2" in testCacheObj.headers);
    assert(testCacheObj.headers["x-custom-header-1"] === "custom-value-1");
    assert(testCacheObj.headers["x-custom-header-2"] === "custom-value-2");
    assert(testCache.stats.hits === 1);
    assert(testCache.stats.misses === 1);
  });

  it("Should allow custom cache key function", async function () {
    app.get(
      "/example-key-func",
      cachedFactory(10, (req) => `${req.originalUrl}-${req.method}`),
      (req, res) => {
        res.send("Custom cache");
      }
    );
    const firstRes = await axios.get("http://127.0.0.1:3000/example-key-func");
    const secondRes = await axios.get("http://127.0.0.1:3000/example-key-func");
    assert(firstRes.status === 200);
    assert(secondRes.status === 200);
    const testCache = app.cache.backend.engine.cache;
    const testCacheObj = JSON.parse(testCache.data["/example-key-func-GET"].v);
    assert(testCacheObj.body === "Custom cache");
    assert(testCache.stats.hits === 1);
    assert(testCache.stats.misses === 1);
  });
});
