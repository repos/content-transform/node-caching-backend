"use strict";
const { describe, beforeEach, afterEach, it } = require("mocha");
const testHelpers = require("../helpers");
const CassandraEngine = require("../../lib/engines/cassandra");
const assert = require("assert");

describe("Cassandra engine tests", function () {
  beforeEach(async function () {
    await testHelpers.setupCassandraEnv();
  });

  afterEach(async function () {
    await testHelpers.tearDownCassandraEnv();
  });

  it("Should initialize engine", async function () {
    const config = testHelpers.getTestCassandraConfig();
    assert.doesNotThrow(async function () {
      const engine = new CassandraEngine(config);
      await engine.client.connect();
      const state = await engine.client.getState();
      assert.ok(state.getConnectedHosts().length > 0);
      engine.client.shutdown();
    });
  });

  it("Should SET key - key doesn't exist", async function () {
    const config = testHelpers.getTestCassandraConfig();
    const engine = new CassandraEngine(config);
    const before = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(before.rowLength === 0);
    await engine.set("testkey", Buffer.from("testvalue"));
    const after = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(after.rowLength === 1);
    assert.ok(after.first().value.toString() === "testvalue");
    engine.client.shutdown();
  });

  it("Should SET key - key doesn't exist - using TTL", async function () {
    this.timeout(10000);
    const config = testHelpers.getTestCassandraConfig();
    const engine = new CassandraEngine(config);
    const before = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(before.rowLength === 0);
    await engine.set("testkey", Buffer.from("testvalue"), 5);
    const after = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(after.rowLength === 1);
    assert.ok(after.first().value.toString() === "testvalue");

    await new Promise((resolve) => setTimeout(resolve, 5000));
    const afterTTL = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(afterTTL.rowLength === 0);

    engine.client.shutdown();
  });

  it("Should SET key - key exists", async function () {
    const config = testHelpers.getTestCassandraConfig();
    const engine = new CassandraEngine(config);
    await engine.set("testkey", Buffer.from("testvalue"));
    const before = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(before.rowLength === 1);
    await engine.set("testkey", Buffer.from("othertestvalue"));
    const after = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(after.rowLength === 1);
    assert.ok(after.first().value.toString() === "othertestvalue");
    engine.client.shutdown();
  });

  it("Should SET key - multiple keys exist", async function () {
    const config = testHelpers.getTestCassandraConfig();
    const engine = new CassandraEngine(config);
    await engine.set("testkey1", Buffer.from("testvalue1"));
    await engine.set("testkey2", Buffer.from("testvalue2"));
    await engine.set("testkey3", Buffer.from("testvalue3"));
    const before = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(before.rowLength === 3);
    await engine.set("testkey1", Buffer.from("othertestvalue"));
    const after = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(after.rowLength === 3);

    const res = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable} WHERE key = 'testkey1'`
    );
    assert.ok(res.first().value.toString() === "othertestvalue");
    engine.client.shutdown();
  });

  it("Should GET key - key exists", async function () {
    const config = testHelpers.getTestCassandraConfig();
    const engine = new CassandraEngine(config);
    await engine.set("testkey", Buffer.from("testvalue"));
    const value = await engine.get("testkey");
    assert.ok(value.toString() === "testvalue");
    engine.client.shutdown();
  });

  it("Should GET key - multiple keys exist", async function () {
    const config = testHelpers.getTestCassandraConfig();
    const engine = new CassandraEngine(config);
    await engine.set("testkey1", Buffer.from("testvalue1"));
    await engine.set("testkey2", Buffer.from("testvalue2"));
    await engine.set("testkey3", Buffer.from("testvalue3"));
    const value = await engine.get("testkey1");
    assert.ok(value.toString() === "testvalue1");
    engine.client.shutdown();
  });

  it("Should GET key - key doesnt exist", async function () {
    const config = testHelpers.getTestCassandraConfig();
    const engine = new CassandraEngine(config);
    await engine.set("testkey1", Buffer.from("testvalue1"));
    await engine.set("testkey2", Buffer.from("testvalue2"));
    await engine.set("testkey3", Buffer.from("testvalue3"));
    const value = await engine.get("nonexistingkey");
    assert.ok(value === null);
    engine.client.shutdown();
  });

  it("Should DELETE key - key exists", async function () {
    const config = testHelpers.getTestCassandraConfig();
    const engine = new CassandraEngine(config);
    await engine.set("testkey", Buffer.from("testvalue"));
    const before = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(before.rowLength === 1);
    await engine.delete("testkey");
    const after = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(after.rowLength === 0);
    engine.client.shutdown();
  });

  it("Should DELETE key - multiples keys exist", async function () {
    const config = testHelpers.getTestCassandraConfig();
    const engine = new CassandraEngine(config);
    await engine.set("testkey1", Buffer.from("testvalue1"));
    await engine.set("testkey2", Buffer.from("testvalue2"));
    await engine.set("testkey3", Buffer.from("testvalue3"));
    const before = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(before.rowLength === 3);
    await engine.delete("testkey1");
    const after = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(after.rowLength === 2);
    const q = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable} WHERE key='testkey1'`
    );
    assert.ok(q.rowLength === 0);
    engine.client.shutdown();
  });

  it("Should DELETE key - key doesn't exist", async function () {
    const config = testHelpers.getTestCassandraConfig();
    const engine = new CassandraEngine(config);
    const before = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(before.rowLength === 0);
    await engine.delete("testkey");
    const after = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(after.rowLength === 0);
    engine.client.shutdown();
  });

  it("Should POP key - key exists", async function () {
    const config = testHelpers.getTestCassandraConfig();
    const engine = new CassandraEngine(config);
    await engine.set("testkey", Buffer.from("testvalue"));
    const before = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(before.rowLength === 1);
    const value = await engine.pop("testkey");
    const after = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(after.rowLength === 0);
    assert.ok(value.toString() === "testvalue");
    engine.client.shutdown();
  });

  it("Should POP key - multiple keys exist", async function () {
    const config = testHelpers.getTestCassandraConfig();
    const engine = new CassandraEngine(config);
    await engine.set("testkey1", Buffer.from("testvalue1"));
    await engine.set("testkey2", Buffer.from("testvalue2"));
    await engine.set("testkey3", Buffer.from("testvalue3"));
    const before = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(before.rowLength === 3);
    const value = await engine.pop("testkey1");
    const after = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(after.rowLength === 2);
    assert.ok(value.toString() === "testvalue1");
    const q = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable} WHERE key='testvalue1'`
    );
    assert.ok(q.rowLength === 0);
    engine.client.shutdown();
  });

  it("Should POP key - key doesn't exist", async function () {
    const config = testHelpers.getTestCassandraConfig();
    const engine = new CassandraEngine(config);
    const before = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(before.rowLength === 0);
    const value = await engine.pop("testkey");
    const after = await engine.client.execute(
      `SELECT * FROM ${config.keyspace}.${config.cacheTable}`
    );
    assert.ok(after.rowLength === 0);
    assert.ok(value === null);
    engine.client.shutdown();
  });
});
