"use strict";
const NodeCache = require("node-cache");

class MemoryEngine {
  constructor(config) {
    this.cache = new NodeCache();
    this.set = this.cache.set;
    this.get = this.cache.get;
    this.pop = this.cache.take;
    this.delete = this.cache.del;
    this.close = this.cache.close;
  }
}

module.exports = MemoryEngine;
