"use strict";

/**
 * Response caching middleware factory
 *
 * @param {number} ttl
 * @param {function} keyFunc
 */
const cachedMiddlewareFactory = (
  ttl = 0,
  keyFunc = (req) => req.originalUrl
) => {
  return async function (req, res, next) {
    const backend = req.app.cache.backend;
    const key = keyFunc(req);
    const value = await backend.get(key);
    if (value) {
      const hitObj = JSON.parse(value);
      res.set(hitObj.headers);
      res.status(hitObj.statusCode);
      res.send(hitObj.body);
      return;
    }

    const cacheObj = {};
    res.sendRes = res.send;
    res.send = async (body) => {
      cacheObj.body = body;
      res.sendRes(body);
    };

    res.on("finish", async function () {
      cacheObj.headers = res.getHeaders();
      cacheObj.statusCode = res.statusCode;
      await backend.set(key, JSON.stringify(cacheObj), ttl);
    });

    next();
  };
};

module.exports = cachedMiddlewareFactory;
